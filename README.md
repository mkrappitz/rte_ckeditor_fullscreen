# CKEditor fullscreen button for TYPO3 12
Offers a fullscreen button to the CKEditor config in TYPO3 12. You can add this button to your toolbar items in your custom rte config / preset. It does not add this button automatically to any of the default rte configs of TYPO3 12. Until TYPO3 11 the fullscreen button in the CKEditor was included in the default rte configs of the RTE. Since TYPO3 12 and due to the update to CKEditor 5 it is not a core feature any more, so this extension brings back the fullscreen button to the CKEditor. The CKEditor plugin itself was taken from https://github.com/leknoppix/ckeditor5-fullscreen and got adapted for usage in TYPO3.

## Config:
There is no config for the extension after installing it.

## Add the button into your own RTE preset
To add this button into  your own RTE preset, import the yaml config in your preset:
```yaml
imports:
    - { resource: "EXT:rte_ckeditor_fullscreen/Configuration/RTE/PluginFullScreen.yaml" }
```

Then insert the button into your toolbar config:
```yaml
  ...
    toolbar:
      items:
          ...
          - fullScreen
          ...
```

## Please give us feedback
We would appreciate any kind of feedback or ideas for further developments to keep improving the extension for your needs.

### Contact me
- [E-Mail](mailto:matthias@aemka.de)
- [Gitlab](https://gitlab.com/mkrappitz/rte_ckeditor_fullscreen)
- [Homepage](https://www.aemka.de)
- [TYPO3.org](https://extensions.typo3.org/extension/rte_ckeditor_fullscreen/)
- [Packagist.org (composer)](https://packagist.org/packages/aemka/typo3-rte-ckeditor-fullscreen)
