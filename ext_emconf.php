<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'CKEditor fullscreen button',
    'description' => 'Adds a fullscreen button option to the CKEditor config in TYPO3 12.',
    'category' => 'be',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'author' => 'Matthias Krappitz',
    'author_email' => 'matthias@aemka.de',
    'version' => '0.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '12.4.0-12.4.99',
            'rte_ckeditor' => '12.4.0-12.4.99',
        ],
        'conflicts' => [],
        'suggests' => [
            'setup' => '',
        ],
    ],
];

