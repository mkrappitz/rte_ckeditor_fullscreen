<?php
return [
    'dependencies' => [
        'backend'
    ],
    'tags' => [
        'backend.form',
    ],
    'imports' => [
        '@aemka/typo3-rte-ckeditor-fullscreen/plugin.js' => 'EXT:rte_ckeditor_fullscreen/Resources/Public/JavaScript/CkeditorPlugins/FullScreen/plugin.js',
    ],
];